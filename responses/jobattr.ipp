GROUP Job-Attributes-Tag

ATTR integer job-id $job
ATTR uri job-uri ipp://$ip:631/jobs/$job
ATTR enum job-state pending
ATTR keyword job-state-reasons printer-stopped
ATTR integer job-impressions-completed 0
ATTR integer job-media-sheets-completed 0
