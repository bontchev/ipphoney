GROUP Operation-Attributes-Tag

ATTR charset attributes-charset utf-8
ATTR language attributes-natural-language en

GROUP Job-Attributes-Tag

ATTR integer job-id $job
ATTR uri job-uri ipp://$ip:631/ipp/print/$job
ATTR enum job-state processing
ATTR keyword job-state-reasons none
ATTR textWithoutLanguage job-state-message Job printing

GROUP End-of-Attributes-Tag
