#!/usr/bin/env bash

set -uo pipefail

version="1.00"

# Set "victim" to the IP address of the target
# or pass it as an argument on the command line
victim=""

port=631
url=/ipp/print
scripts=./scripts
ippopts=t4
testfile=testfile.jpg
testuri=https://bontchev.nlcv.bas.bg/nospam.gif

error()
{
    echo "$1" 1>&2
    exit 1
}

ip_valid()
{
    if [[ $1 =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
    then
        local ip
        local IFS='.'
        read -r -a ip <<< "$1"
        for quad in {0..3}
        do
            [[ "${ip[$quad]}" -gt 255 ]] && return 1
        done
        return 0
    fi
    return 1
}

check_vars()
{
    has_warnings=0
    if [ -z "$victim" ]
    then
        echo "The variable \$victim (containing the target IP) is not set."
        echo "Either set it, or supply the IP address of the targed as a"
        echo "command-line argument. Aborting."
        exit 1
    fi
    if ! ip_valid "$victim"
    then
        error "targetIP does not contain a valid IP address, aborting."
    fi
    if [ -z "${port+x}" ]
    then
        echo "The variable \$port is not set, assuming \"631\"."
        has_warnings=1
        port=631
    fi
    case $port in
        ''|*[!0-9]*)
            error "port must be a number"
            ;;
        *)
            ;;
    esac
    if [ -z "${url+x}" ]
    then
        echo "The variable \$url is not set, assuming \"/ipp/print\"."
        has_warnings=1
        url=/ipp/print
    fi
    if [ -z "${scripts+x}" ]
    then
        echo "The variable \$scripts is not set, assuming \"./scripts\"."
        has_warnings=1
        scripts=./scripts
    fi
    if [ -z "${ippopts+x}" ]
    then
        echo "The variable \$ippopts is not set, assuming \"-t4\"."
        has_warnings=1
        ippopts="t4"
    fi
    if [ ! -r "$scripts/$testfile" ]
    then
        error "Can't access file \"$scripts/$testfile\"."
    fi
    if [ "$has_warnings" -gt 0 ]
    then
        pause
    fi
}

clear()
{
    echo -ne '\033c'
}

pause()
{
    read -rp "Press [Enter] key to continue..."
}

show_menu()
{
    clear
    echo '====== IPP honeypot test =========='
    echo "Testing target at ${victim}${port}"
    echo '-----------------------------------'
    echo '0. All'
    echo '1. Get-Printer-Attributes'
    echo '2. Get-Jobs'
    echo '3. Get completed jobs'
    echo '4. Print-Job'
    echo '5. Print-Uri'
    echo '6. Validate-Job'
    echo '7. Pause-Printer and Resume-Printer'
    echo '8. Cancel-Job'
    echo '9. Cancel current job'
    echo 'A. Get-Job-Attributes'
    echo 'B. Create-Job and Send-Document'
    echo 'C. Create-Job and Send-Uri'
    echo '-----------------------------------'
    echo "====== Enter 'Q' to quit =========="
    echo ""
}

check_file()
{
    for file in "$@"
    do
        if ! [ -r "$file" ]
        then
            error "Can't access file \"$file\", aborting."
        fi
    done
    return 0
}

get_printer_attributes()
{
    echo 'Testing Get-Printer-Attributes...'
    check_file "$scripts/get-printer-attributes.test" || return
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-printer-attributes.test"
}

get_jobs()
{
    echo 'Testing Get-Jobs...'
    check_file "$scripts/get-jobs.test" || return
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"
}

get_completed_jobs()
{
    echo 'Testing Get completed jobs...'
    check_file "$scripts/get-completed-jobs.test" || return
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-completed-jobs.test"
}

print_job()
{
    echo 'Testing Print-Job...'
    check_file "${scripts}/print-job.test" "${scripts}/${testfile}" || return
    ipptool "$ippopts" -f "$testfile" "ipp://${victim}${port}${url}" "${scripts}/print-job.test"
}

print_uri()
{
    echo 'Testing Print-Uri...'
    check_file "$scripts/print-uri.test" || return
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"
}

validate_job()
{
    echo 'Testing Validate-Job...'
    check_file "$scripts/validate-job.test" || return
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/validate-job.test"
}

pause_resume()
{
    echo 'Testing Pause-Printer and Resume-Printer...'

    check_file "${scripts}/get-printer-attributes.test" "${scripts}/pause-printer.test" "${scripts}/print-uri.test" "${scripts}/get-jobs.test" "${scripts}/resume-printer.test" || return

    echo "Getting baseling printer attributes:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-printer-attributes.test"

    echo "Pausing the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/pause-printer.test"

    echo "Getting printer attributes (check if paused):"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-printer-attributes.test"

    echo "Sending a printing job while printer is paused:"
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"

    echo "Checking that the job list is not empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Resuming the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/resume-printer.test"

    echo "Checking that the job list is empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Getting printer attributes (check if idle):"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-printer-attributes.test"
}

cancel_job()
{
    echo 'Testing Cancel-Job...'

    check_file "${scripts}/pause-printer.test" "${scripts}/print-uri.test" "${scripts}/get-jobs.test" "${scripts}/cancel-job.test" "${scripts}/resume-printer.test" || return

    echo "Pausing the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/pause-printer.test"

    echo "Checking that the job list is empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Sending a printing job while printer is paused:"
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"

    echo "Sending a second printing job while printer is paused:"
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"

    echo "Checking that the job list is not empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Trying to cancel non-existent job #3 (should fail):"
    ipptool "$ippopts" -d job-id=3 "ipp://${victim}${port}${url}" "${scripts}/cancel-job.test"

    echo "Trying to cancel job #2 (should pass):"
    ipptool "$ippopts" -d job-id=2 "ipp://${victim}${port}${url}" "${scripts}/cancel-job.test"

    echo "Checking that job list is not empty and contains only 1 job:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Resuming the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/resume-printer.test"
}

cancel_current_job()
{
    echo 'Testing Cancel current job...'

    check_file "${scripts}/pause-printer.test" "${scripts}/print-uri.test" "${scripts}/get-jobs.test" "${scripts}/cancel-current-job.test" "${scripts}/resume-printer.test" || return

    echo "Pausing the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/pause-printer.test"

    echo "Checking that the job list is empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Sending a printing job while printer is paused:"
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"

    echo "Checking that the job list is not empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Canceling the current job:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/cancel-current-job.test"

    echo "Checking that job list is not empty and contains only 1 job:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Resuming the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/resume-printer.test"
}

get_job_attributes()
{
    echo 'Testing Get-Job-Attributes...'

    check_file "${scripts}/pause-printer.test" "${scripts}/print-uri.test" "${scripts}/get-jobs.test" "${scripts}/get-job-attributes.test" "${scripts}/resume-printer.test" || return

    echo "Pausing the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/pause-printer.test"

    echo "Checking that the job list is empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Sending a printing job while printer is paused:"
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"

    echo "Sending a second printing job while printer is paused:"
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/print-uri.test"

    echo "Checking that the job list is not empty:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/get-jobs.test"

    echo "Trying to get the attributes of non-existent job #3 (should fail):"
    ipptool "$ippopts" "ipp://${victim}${port}/jobs/3" "${scripts}/get-job-attributes.test"

    echo "Getting the job attributes of job #2 (should pass):"
    ipptool "$ippopts" "ipp://${victim}${port}/jobs/2" "${scripts}/get-job-attributes.test"

    echo "Resuming the printer:"
    ipptool "$ippopts" "ipp://${victim}${port}${url}" "${scripts}/resume-printer.test"
}

send_document()
{
    echo 'Testing Create-Job and Send-Document...'
    check_file "${scripts}/create-job.test" "${scripts}/${testfile}" || return
    ipptool "$ippopts" -f "$testfile" "ipp://${victim}${port}${url}" "${scripts}/create-job.test"
}

send_uri()
{
    echo 'Testing Create-Job and Send-Uri..'
    check_file "$scripts/send-uri.test" || return
    ipptool "$ippopts" -d "document-uri=$testuri" "ipp://${victim}${port}${url}" "${scripts}/send-uri.test"
}

read_options()
{
    local choice
    read -rp "Enter choice [ 1 - C or Q] " choice
    case $choice in
        0)
            get_printer_attributes
            get_jobs
            get_completed_jobs
            print_job
            print_uri
            validate_job
            pause_resume
            cancel_job
            cancel_current_job
            get_job_attributes
            send_document
            send_uri
            ;;
        1)
            get_printer_attributes
            ;;
        2)
            get_jobs
            ;;
        3)
            get_completed_jobs
            ;;
        4)
            print_job
            ;;
        5)
            print_uri
            ;;
        6)
            validate_job
            ;;
        7)
            pause_resume
            ;;
        8)
            cancel_job
            ;;
        9)
            cancel_current_job
            ;;
        [Aa])
            get_job_attributes
            ;;
        [Bb])
            send_document
            ;;
        [Cc])
            send_uri
            ;;
        [Qq])
            echo "Exiting."
            exit 0
            ;;
        *)
            echo "Invalid choice $choice"
            ;;
    esac
}

usage()
{
    echo "Usage: $(basename "$0") [options] targetIP" 1>&2
    echo -e "\t-h\t\tDisplay help and exit" 1>&2
    echo -e "\t-v\t\tDisplay version number and exit" 1>&2
    echo -e "\t-p port\t\tPort(default: $port)" 1>&2
    echo -e "\t-u URL\t\tPath (default: $url)" 1>&2
    echo -e "\t-s scriptdir\tScripts directory (default: $scripts)" 1>&2
    echo -e "\t-o ippopts\tipptool options (default: $ippopts)" 1>&2
    echo -e "\t-f file\t\tTest file to print (default: $testfile)" 1>&2
    echo -e "\t-r URL\t\tTest URL to print" 1>&2
    echo -e "\t\t\t(default: $testuri)" 1>&2
    exit 1
}

trap '' SIGINT SIGQUIT SIGTSTP

while getopts ":hvp:u:s:o:f:r:" options
do
    case "${options}" in
        h)
            usage
            ;;
        p)
            port=${OPTARG}
            case $port in
                ''|*[!0-9]*)
                    error "port must be a number"
                    ;;
                *)
                    ;;
            esac
            ;;
        u)
            url=${OPTARG}
            ;;
        s)
            scripts=${OPTARG}
            if [ ! -d "$scripts" ]
            then
                error "Directory \"$scripts\" doesn't exist"
            fi
            ;;
        o)
            ippopts=${OPTARG}
            ;;
        f)
            testfile=${OPTARG}
            if [ ! -r "$scripts/$testfile" ]
            then
                error "The file \"$scripts/$testfile\" doesn't exist or isn't readable"
            fi
            ;;
        r)
            testuri=${OPTARG}
            ;;
        v)
            echo "$(basename "$0") version $version" 1>&2
            exit
            ;;
        :)
            error "Error: -${OPTARG} requires an argument."
            ;;
        ?)
            error "Invalid option -${OPTARG}"
            ;;
        *)
            error "Error processing the options"
            ;;
    esac
done

shift $((OPTIND-1))

if [ $# -gt 0 ]
then
    victim=$1
else
    echo "targetIP not specified" 1>&2
    usage
fi

check_vars

port=":$port"
ippopts="-$ippopts"

while true
do
    show_menu
    read_options
    pause
done
