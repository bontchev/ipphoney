# Sending the output of the honeypot to a Discord channel

This guide describes how to send the reports from the honeypot to a Discord
channel.

- [Sending the output of the honeypot to a Discord channel](#sending-the-output-of-the-honeypot-to-a-discord-channel)
  - [Prerequisites](#prerequisites)
  - [Select your Discord server](#select-your-discord-server)
  - [Create a channel for the honeypot's reports](#create-a-channel-for-the-honeypots-reports)
  - [Configure the honeypot](#configure-the-honeypot)

## Prerequisites

- Working honeypot installation
- A Discord account

## Select your Discord server

- Log into your Discord account
- If you don't have your own server yet, create one by cling the "+" icon on
the bar on the left. The newly created server will appear on that bar.
- Select your server by clicking its icon on the bar on the left.

## Create a channel for the honeypot's reports

- On the leftmost column of the page (to the right of the bar where the
server's icon is), select the "+" sign in the "Text Channels" section.
- Give the channel a name suggesting that the honeypot's reports will
appear there - e.g., `ipphoney`. The channel will appear in the "Text
Channels" section.
- Click the channel's name and select the gear icon to open the "Edit
Channel" dialog.
- Click "Integrations" on the left, in order to switch to the channel's
integrations page.
- Click on the "Webhooks" button and then on "New Webhook". By default, the
new webhook will be named "Captain Hook".
- Click the newly created webhook to change is properties. You can change
its name to something more meaningful, like `IPPHoney` - it will be the name
of the "user" posting to the channel.
- While the webhook's properties are still open, click on the "Copy Webhook
URL" button. This will copy the URL that you need to put in the honeypot's
configuration section for Discord.
- Press Esc to close the editor of the channel's properties. You can leave
Discord now.

## Configure the honeypot

- Stop the honeypot, if it is running.
- Open the file `etc/honeypot.cfg` and uncomment the `[output_discord]`
section in it.
- Set the variable `url` webhook URL that you have obtained in the previous
section.
- Optionally, set the variable `delay` to something different than the default
value of `2.0`. This is the delay, in seconds, between any two messages sent
to the bot. Note that if this number too small, Discord's rate
limiting will cause it to return `429 Too many requests` errors.
- Set the variable `enabled` to `true`.
- Launch the honeypot.
