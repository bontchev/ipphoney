# Planned Future Improvements

* Additional output plugins for:
  * Influx
  * RethinkDB
* Emulation of additional IPP operations:
  * `Hold-Job`
  * `Release-Job`
  * `Create-Printer-Subscriptions`
  * `Disable-Printer`
  * `Enable-Printer`
  * `Get-Notifications`
  * `Get-Printers`
  * `Get-Subscriptions`
  * `Get-System-Attributes`
  * `Set-System-Attributes`
* Make the honeypot installable with `pip`
