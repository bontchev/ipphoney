# Sending the output of the honeypot to a Slack channel

This guide describes how to send the reports from the honeypot to a Slack
channel.

## Prerequisites

- Working honeypot installation
- A Slack account (workspace)

## Create a channel for the honeypot's reports

- Go to [Slack](https://slack.com) and log into your account (workspace) there.
- Click the "LAUNCH SLACK" button. Slack will open in a new browser tab.
- On the vertical planel on the left, click on the *right* side of the word
  `Channels` and select **Create**->**Create channel** from the menu that
  appears.
- Enter some descriptive name, e.g., `ipphoney` in the field `Name` on the
  dialog that appears.
- Click the **Next** button.
- Select the kind of channel you want - public or private on the dialog that
  appears.
- Click the **Create** button.

## Create an app (bot)

- Open a new browser tab and go to the URL
  [https://api.slack.com/apps](https://api.slack.com/apps). You
  have to do it manually; there doesn't seem to be a way to go there from
  Slack's user interface.
- Click on the big green **Create New App** button on the right.
- Choose to create an app **From scratch** on the dialog that appears.
- Enter the app name (e.g., `Honeypot Reporter`) and select your workspace,
  then click on the **Create App** button. This will take you to the app
  configuration page.
- [OPTIONAL] In the dialog at the very bottom of the **Basic Information**
  section, enter a short description for your bot (e.g., "Reports connections
  to our honeypots"), choose a background color (e.g., `#2C2D30`; this field
  cannot be empty), and choose an image (size must be between 512x512 and
  2000x2000) for your bot's icon, then click the **Save Changes** button at
  the bottom.
- Go to the **OAuth & Permissions** section in the vertical bar on the right.
- Near the bottom, in the **Scopes** section, click on the **Add an OAuth Scope**
  button and select **chat:write** from the dialog that appears. Your bot does
  not need any other permission.
- A bit higher on the page (still on the **OAuth & Permissions** page), go
  to the **OAuth Tokens for Your Workspace** section and click on the
  **Install to Workspace** button. You'll be asked for confirmation; allow it.
- A new field labeled **Bot User OAuth Token** will appear in the section
  **OAuth Tokens for Your Workspace**. Copy it; you'll have to put it in the
  honeypot's config file.

## Add the bot to the channel

- Go back to the browser tab with the Slack application.
- Select the created channel `#ipphoney`.
- Enter the command to invite your bot (`/invite @honeypot_reporter`) in the
  area where you normally enter messages to be posted to the channel.

## Configure the honeypot to use the bot

- Uncomment the `[output_slack]` section in the file `honeypot.cfg`.
- Set the variable `channel` to the name of the channel you created (e.g.,
  `ipphoney`).
- Set the variable `token` to the bot token you copied when creating the bot.
- Set the variable `enabled` to `true`.
- Save the file.
- Launch the honeypot.
