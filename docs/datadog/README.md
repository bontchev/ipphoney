# Sending the Output of the Honeypot to a Datadog Log Management Account

This guide describes how to configure and send cowrie outputs to Datadog Log Management.

- [Sending the Output of the Honeypot to a Datadog Log Management Account](#sending-the-output-of-the-honeypot-to-a-datadog-log-management-account)
  - [Prerequisites](#prerequisites)
  - [Configuration of the Datadog output module](#configuration-of-the-datadog-output-module)
  - [Datadog Configuration](#datadog-configuration)

## Prerequisites

- Working honeypot installation
- Existing Datadog account.

## Configuration of the Datadog output module

- Modify the file `etc/honeypot.cfg` and uncomment the `[output_datadog]` section.
- Set the `url` variable to `https://http-intake.logs.datadoghq.eu/api/v2/logs`, if
you're in the EU - or to `https://http-intake.logs.datadoghq.com/api/v2/logs`, if
you reside elsewhere.
- Add an API Key. You may generate one for your organisation from
[here](https://app.datadoghq.eu/organization-settings/api-keys), if you're in the
EU, or from [here](https://app.datadoghq.eu/organization-settings/api-keys), if
you reside elsewhere.
- Optionally customize the variables `ddsource`, `ddtags`, `service` and `hostname`.
Otherwise, the defaults are respectively `ipphoney`, `env:prod`, `honeypot` and the
hostname of the machine, running the honeypot.
- Set the variable `enabled` to `true`.

## Datadog Configuration

JSON logs are handled without further configuration in Datadog.
