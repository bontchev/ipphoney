
from time import time
from uuid import uuid4
from requests import get
from hashlib import sha256
from sys import version_info
from datetime import datetime
from os.path import join, exists
from os import SEEK_SET, SEEK_END
from ipaddress import ip_address, ip_network

from core.compile import compile_line
from core.tools import (
    mkdir,
    decode,
    hexdump,
    get_word,
    get_dword,
    get_string,
    get_real_ip,
    get_local_ip,
    get_real_port,
    get_utc_time,
    print_query,
    write_event
)

from pytz import timezone

from twisted.python.log import msg
from twisted.web.resource import Resource

try:
    from urllib.parse import unquote
except ImportError:
    from urlparse import unquote    # type: ignore


if version_info[0] >= 3:
    def ord(x):
        return x
    def unicode(x):
        return x


class Index(Resource):
    isLeaf = True
    page_cache = {
        'getattr.ipp': b'',
        'getjobs.ipp': b'',
        'printjob.ipp': b'',
        'getjattr.ipp': b'',
        'header.ipp' : b'',
        'jobattr.ipp': b''
    }

    def __init__(self, options):
        self.cfg = options

    def render_HEAD(self, request):
        self.log_request(request)
        self.report_event(request)
        return self.send_response(request)

    def render_GET(self, request):
        self.log_request(request)
        self.report_event(request)
        return self.send_response(request)

    def render_POST(self, request):
        self.log_request(request)
        if request.getHeader('Content-Length'):
            content_length = int(request.getHeader('Content-Length'))
        else:
            old_pos = request.content.tell()
            request.content.seek(0, SEEK_END)
            content_length = request.content.tell()
            request.content.seek(old_pos, SEEK_SET)
        if content_length:
            post_data = request.content.read()
            answer = self.process_request(request, post_data)
            return self.send_response(request, answer)
        return self.send_response(request)

    def logger(self, message, ip):
        for network in self.cfg['blacklist']:
            if ip_address(unicode(ip)) in ip_network(unicode(network)):
                return
        msg(message)

    def log_request(self, request):
        ip = request.getClientAddress().host
        path = unquote(decode(request.uri))
        method = decode(request.method)
        self.logger('[INFO] {}: {} {}'.format(ip, method, path), ip)

    def process_request(self, request, data):
        from core.data import (
            operations_code,
            attributes_code,
            groups_code,
            operations,
            attributes,
            groups
        )

        def error(query, answer):
            msg('\n' + print_query(query))
            return answer

        answer = b''
        if len(data) < 9:
            return compile_line('VERSION 1.1', self.cfg) + compile_line('STATUS successful-ok', self.cfg)
        request_id = 0
        query = {}
        index = 0
        version_major = ord(data[index])
        index += 1
        version_minor = ord(data[index])
        index += 1
        answer += compile_line('VERSION {}.{}'.format(version_major, version_minor), self.cfg)
        answer += compile_line('STATUS successful-ok', self.cfg)
        query['version'] = '{}.{}'.format(version_major, version_minor)
        operation = get_word(data, index)
        index += 2
        request_id = get_dword(data, index)
        index += 4
        answer += compile_line('REQUEST-ID {:08X}'.format(request_id), self.cfg)
        query['request_id'] = '{:08X}'.format(request_id)
        if operation not in operations_code:
            return error(query, answer)
        query['operation'] = operations_code[operation]
        # begin-attribute-group-tag must follow:
        group_id = ord(data[index])
        if group_id not in groups_code:
            return error(query, answer)
        index += 1
        query['groups'] = []
        group = {}
        group['group_type'] = groups_code[group_id]
        group['attributes'] = []
        query['groups'].append(group)
        data_len = len(data)
        col_value = b''
        in_collection = 0
        in_list = False
        while index < data_len:
            attr = ord(data[index])
            index += 1
            if attr == groups['end-of-attributes-tag']:
                break
            elif attr in groups_code:
                group = {}
                group['group_type'] = groups_code[attr]
                group['attributes'] = []
                query['groups'].append(group)
                continue
            if attr not in attributes_code:
                return error(query, answer)
            if index + 2 >= data_len:
                break
            name_len = get_word(data, index)
            index += 2
            if name_len:
                if index + name_len >= data_len:
                    break
                name = get_string(data, index, name_len)
                index += name_len
            elif attr != attributes['memberattrname'] and in_collection == 0:
                in_list = True
            if index + 2 >= data_len:
                break
            value_len = get_word(data, index)
            index += 2
            if index + value_len >= data_len:
                break
            if attr == attributes['boolean']:
                value = 'true' if ord(data[index]) else 'false'
                index += 1
            elif attr == attributes['integer']:
                value = get_dword(data, index)
                index += 4
            elif attr == attributes['rangeofinteger']:
                lower_limit = get_dword(data, index)
                index += 4
                upper_imit = get_dword(data, index)
                index += 4
                value = str(lower_limit) + '-' + str(upper_imit)
            elif attr in [attributes['unknown'], attributes['no-value']]:
                value = attributes_code[attr]
            elif attr == attributes['resolution']:
                value = str(get_dword(data, index))
                index += 8
                value += 'dpi' if ord(data[index]) == 0x03 else 'dpcm'
                index += 1
            elif attr == attributes['enum']:
                enum_code = get_dword(data, index)
                index += 4
                if enum_code in operations_code:
                    value = operations_code[enum_code]
                else:
                    value = '{:04X}'.format(enum_code)
            elif attr == attributes['datetime']:
                year = get_word(data, index)
                month = ord(data[index + 2])
                day = ord(data[index + 3])
                hour = ord(data[index + 4])
                minute = ord(data[index + 5])
                second = ord(data[index + 6])
                value = datetime.strftime(datetime(year, month, day, hour, minute, second), '%Y-%m-%dT%H:%M:%SZ')
                index += 11
            elif attr == attributes['collection']:
                if in_collection:
                    col_value += b'{'
                else:
                    col_value = b'{'
                    in_collection += 1
                value = ''
            elif attr == attributes['memberattrname']:
                member_name_len = value_len
                member_name = get_string(data, index, member_name_len)
                col_value += b' ' + member_name + b'='
                value = ''
                index += member_name_len
            elif attr == attributes['endcollection']:
                col_value += b' }'
                in_collection -= 1
                if in_collection:
                    value = ''
                else:
                    value = col_value.replace('{ ', '{').replace(' }', '}')
                    value_len = len(value)
                    attr = attributes['collection']
            else:
                # deleteAttribute
                # adminDefine
                # octetString
                # textWithLanguage
                # nameWithLanguage
                # textWithoutLanguage
                # nameWithoutLanguage
                # uri
                # uriScheme
                # charset
                # naturalLanguage
                # mimeMediaType
                value = get_string(data, index, value_len)
                if not value:
                    value = "''"
                index += value_len
            if in_collection:
                col_value += str(value)
            elif in_list:
                query['groups'][-1]['attributes'][-1]['attribute_value'].append(value)
                in_list = False
            else:
                attribute = {}
                attribute['attribute_type'] = attributes_code[attr]
                attribute['attribute_name'] = name
                attribute['attribute_value'] = [value]
                query['groups'][-1]['attributes'].append(attribute)
        file_size = 0
        if operation in [operations['print-job'], operations['send-document']]:
            file_size = self.do_print_job(request, query, data[index:])
        elif operation in [operations['print-uri'], operations['send-uri']]:
            file_size = self.do_print_uri(request, query)
        else:
            self.report_event(request, query)
        ip = request.getClientAddress().host
        return self.process_operation(data, query, answer, file_size, ip)

    def save_file(self, request, query, document, doc_size):
        hash = sha256(document).hexdigest()
        filename = join(self.cfg['download_dir'], hash)
        mkdir(self.cfg['download_dir'])
        if not exists(filename):
            with open(filename, 'wb') as f:
                f.write(document)
        file_info = {}
        file_info['filename'] = filename
        file_info['filesize'] = doc_size
        file_info['hash'] = hash
        self.report_event(request, query, file_info)

    def get_attribute(self, attrib, query):
        for group in query['groups']:
            if group['group_type'] == 'Operation-Attributes-Tag':
                for attribute in group['attributes']:
                    if attribute['attribute_name'] == attrib:
                        return attribute['attribute_value']
        return None

    def do_print_job(self, request, query, document):
        doc_size = len(document)
        max_size = self.cfg['download_limit_size']
        size_ok = max_size == 0 or doc_size <= max_size
        if self.cfg['download_files'] and size_ok:
            self.save_file(request, query, document, doc_size)
        else:
            self.report_event(request, query)
        return doc_size

    def do_print_uri(self, request, query):
        uri = self.get_attribute('document-uri', query)
        uri = b'' if uri is None else uri[0]
        ip = request.getClientAddress().host
        self.logger('[INFO] URL: {}'.format(uri), ip)
        doc_size = 0
        try:
            remote_doc = get(uri, allow_redirects=True, stream=True)
            remote_doc.raise_for_status()
            doc_size = int(remote_doc.headers.get('content-length'))
            self.logger('[INFO] Remote doc size: {}'.format(doc_size), ip)
            max_size = self.cfg['download_limit_size']
            size_ok = max_size == 0 or doc_size <= max_size
            if self.cfg['download_files'] and size_ok:
                document = b''
                for chunk in remote_doc.iter_content(chunk_size=1024):
                    if chunk:
                        document += chunk
                self.save_file(request, query, document, doc_size)
            else:
                self.report_event(request, query)
        except Exception as e:
            msg('Error: {}'.format(e))
            self.report_event(request, query)
        return doc_size

    def process_operation(self, data, query, answer, file_size, ip):
        def do_get_jobs(query):
            if len(self.cfg['jobs']):
                answer = self.get_page('header.ipp')
                limit = self.get_attribute('limit', query)
                limit = len(self.cfg['jobs']) if limit is None else limit[0]
                for index in range(limit):
                    self.cfg['current_job'] = self.cfg['jobs'][index]['id']
                    self.page_cache['job_attr.ipp'] = b''
                    answer += self.get_page('jobattr.ipp')
                answer += compile_line('GROUP End-of-Attributes-Tag', self.cfg)
                return answer
            else:
                return self.get_page('getjobs.ipp')

        def get_job_id(query):
            uri = self.get_attribute('job-uri', query)
            job_id = None
            if uri is not None:
                job_id = int(uri[0].split('/')[-1])
            else:
                job_id = self.get_attribute('job-id', query)
                if job_id is not None:
                    job_id = int(job_id[0])
            return job_id

        def do_cancel_job(query, answer):
            job_id = get_job_id(query)
            found = False
            for job in self.cfg['jobs']:
                if job_id == job['id']:
                    self.cfg['jobs'].remove(job)
                    found = True
                    break
            if not found:
                answer = answer[0:2] + compile_line('STATUS client-error-not-possible', self.cfg) + answer[4:]
            answer += self.get_page('getjobs.ipp')
            return answer

        def do_send_document(query, answer, file_size):
            job_id = get_job_id(query)
            found = False
            for job in self.cfg['jobs']:
                if job_id == job['id']:
                    self.cfg['current_job'] = job_id
                    self.page_cache['getjattr.ipp'] = b''
                    answer += self.get_page('getjattr.ipp')
                    if self.cfg['state'] == 'idle':
                        self.cfg['jobs'].remove(job)
                    found = True
                    break
            if not found:
                answer = answer[0:2] + compile_line('STATUS client-error-not-possible', self.cfg) + answer[4:]
                answer += self.get_page('getjobs.ipp')
            return answer

        def do_get_job_attr(query, answer):
            job_id = get_job_id(query)
            found = False
            for job in self.cfg['jobs']:
                if job_id == job['id']:
                    self.cfg['current_job'] = job_id
                    answer += compile_line('GROUP Operation-Attributes-Tag', self.cfg)
                    answer += compile_line('ATTR charset attributes-charset utf-8', self.cfg)
                    answer += compile_line('ATTR language attributes-natural-language en', self.cfg)
                    answer += compile_line('GROUP Job-Attributes-Tag', self.cfg)
                    answer += compile_line('ATTR integer number-of-documents 1', self.cfg)
                    answer += compile_line('ATTR integer job-media-progress 0', self.cfg)
                    answer += compile_line('ATTR uri job-more-info http://$ip:631/ipp/print/$job', self.cfg)
                    answer += compile_line('ATTR integer job-printer-up-time {}'.format(int(time())), self.cfg)
                    answer += compile_line('ATTR uri job-printer-uri ipp://$ip:631/ipp/print', self.cfg)
                    answer += compile_line('ATTR uri job-uri ipp://$ip/jobs/$job', self.cfg)
                    answer += compile_line('ATTR uri printer-uri ipp://$ip:631/ipp/print', self.cfg)
                    if job['format'] is not None:
                        answer += compile_line('ATTR mimeMediaType document-format-detected {}'.format(job['format']), self.cfg)
                        answer += compile_line('ATTR mimeMediaType document-format {}'.format(job['format']), self.cfg)
                    answer += compile_line('ATTR integer job-priotiry 50', self.cfg)
                    answer += compile_line('ATTR uri job-uuid urn:uuid:{}'.format(job['uuid']), self.cfg)
                    answer += compile_line('ATTR no-value date-time-at-completed no-value', self.cfg)
                    answer += compile_line('ATTR dateTime date-time-at-creation {}'.format(job['timestamp']), self.cfg)
                    answer += compile_line('ATTR no-value date-time-at-processing no-value', self.cfg)
                    answer += compile_line('ATTR no-value time-at-completed no-value', self.cfg)
                    answer += compile_line('ATTR integer time-at-creation {}'.format(int(time())), self.cfg)
                    answer += compile_line('ATTR no-value time-at-processing no-value', self.cfg)
                    answer += compile_line('ATTR integer job-id $job', self.cfg)
                    answer += compile_line('ATTR enum job-state pending', self.cfg)
                    answer += compile_line('ATTR keyword job-state-reasons paused', self.cfg)
                    answer += compile_line('ATTR integer job-impressions-completed 0', self.cfg)
                    answer += compile_line('ATTR integer job-media-sheets-completed 0', self.cfg)
                    if 'size' in job:
                        answer += compile_line('ATTR integer job-k-octets {}'.format(job['size']), self.cfg)
                    answer += compile_line('ATTR keyword job-hold-until no-hold', self.cfg)
                    answer += compile_line('ATTR nameWithoutLanguage job-sheets none,none', self.cfg)
                    answer += compile_line('GROUP End-of-Attributes-Tag', self.cfg)
                    found = True
                    break
            if not found:
                answer = answer[0:2] + compile_line('STATUS client-error-not-possible', self.cfg) + answer[4:]
                answer += self.get_page('getjobs.ipp')
            return answer

        def do_print(query, file_size):
            def ceildiv(n, d):
                return -(n // -d)

            if self.cfg['state'] == 'idle':
                return self.get_page('printjob.ipp')
            else:
                job = {}
                job['id'] = self.cfg['job_index']
                job['format'] = self.get_attribute('document-format', query)
                job['uuid'] = str(uuid4())
                job['timestamp'] = datetime.now(tz=timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%SZ')
                job['size'] = ceildiv(file_size, 1024)
                self.cfg['jobs'].append(job)
                self.page_cache['printjob.ipp'] = b''
                answer = self.get_page('printjob.ipp')
                self.cfg['job_index'] += 1
                return answer

        def do_create_job(query):
            job = {}
            job['id'] = self.cfg['job_index']
            job['format'] = self.get_attribute('document-format', query)
            job['uuid'] = str(uuid4())
            job['timestamp'] = datetime.now(tz=timezone('UTC')).strftime('%Y-%m-%dT%H:%M:%SZ')
            self.cfg['jobs'].append(job)
            self.cfg['current_job'] = job['id']
            self.page_cache['getjattr.ipp'] = b''
            answer = self.get_page('getjattr.ipp')
            self.cfg['job_index'] += 1
            return answer

        def do_pause():
            self.cfg['state'] = 'stopped'
            self.page_cache['getattr.ipp'] = b''
            return self.get_page('getjobs.ipp')

        def do_resume():
            self.cfg['state'] = 'idle'
            self.cfg['jobs'] = []
            self.cfg['job_index'] = 1
            self.page_cache['getattr.ipp'] = b''
            return self.get_page('getjobs.ipp')

        operation = query['operation']
        self.logger('[INFO] Operation: {}'.format(operation), ip)
        if operation == 'Get-Printer-Attributes':
            answer += self.get_page('getattr.ipp')
        elif operation == 'Get-Job-Attributes':
            answer = do_get_job_attr(query, answer)
        elif operation == 'Get-Jobs':
            answer += do_get_jobs(query)
        elif operation == 'Cancel-Job':
            answer = do_cancel_job(query, answer)
        elif operation == 'Validate-Job':
            answer += self.get_page('getjobs.ipp')
        elif operation == 'Create-Job':
            answer += do_create_job(query)
        elif operation in ['Print-Job', 'Print-Uri']:
            answer += do_print(query, file_size)
        elif operation in ['Send-Document', 'Send-Uri']:
            answer = do_send_document(query, answer, file_size)
        elif operation == 'Pause-Printer':
            answer += do_pause()
        elif operation == 'Resume-Printer':
            answer += do_resume()
        else:
            self.logger('[INFO] POST body:\n{}'.format(hexdump(data)), ip)
            answer += self.get_page('getjobs.ipp')
        if self.cfg['verbose']:
            self.logger('\n' + print_query(query), ip)
        return answer

    def report_event(self, request, query=None, file_info=None):
        unix_time = time()
        event = {}
        if query is None or 'operation' not in query:
            event['eventid'] = 'ipphoney.connect'
        else:
            event['eventid'] = 'ipphoney.' + query['operation'].lower()
            event['operation'] = query['operation']
        event['timestamp'] = get_utc_time(unix_time)
        event['unixtime'] = unix_time
        event['url'] = unquote(decode(request.uri))
        event['src_ip'] = get_real_ip(request)
        event['src_port'] = get_real_port(request)
        event['dst_port'] = self.cfg['port']
        event['sensor'] = self.cfg['sensor']
        event['request'] = decode(request.method)
        if file_info is not None:
            event['filename'] = file_info['filename']
            event['filesize'] = file_info['filesize']
            event['sha256'] = file_info['hash']
        if query is not None:
            event['query'] = query
        user_agent = request.getHeader('User-Agent')
        if user_agent:
            event['user_agent'] = user_agent
        event['dst_ip'] = self.cfg['public_ip'] if self.cfg['report_public_ip'] else get_local_ip()
        write_event(event, self.cfg)

    # a simple wrapper to cache files from "responses" folder
    def get_page(self, page):
        if page not in self.page_cache:
            msg('Missing file: "{}".'.format(page))
            return ''
        # if page is not in cache, load it from file
        if self.page_cache[page] == b'':
            with open(join(self.cfg['responses_dir'], page), 'r') as f:
                lines = f.readlines()
                for line in lines:
                    line = line.strip()
                    if not line or line[0] == '#':
                        continue
                    self.page_cache[page] += compile_line(line, self.cfg)
        return self.page_cache[page]

    def send_response(self, request, page=''):
        request.setHeader('Date', datetime.now(tz=timezone('UTC')).strftime('%a, %d %b %Y %H:%M:%S GMT'))
        request.setHeader('Server', 'Lexmark_Web_Server')
        request.setHeader('Cache-Control', 'no-cache')
        request.setHeader('X-Frame-Options', 'SAMEORIGIN')
        request.setHeader('Upgrade', 'TLS/1.0, HTTP/1.1')
        request.setHeader('Connection', 'upgrade')
        request.setHeader('Content-Length', str(len(page)))
        request.setHeader('X-XSS-Protection', '1; mode=block')
        request.setHeader('Content-Security-Policy', "frame-ancestors 'none'")
        request.setHeader('X-Content-Type-Options', 'nosniff')
        request.setHeader('Content-type', 'application/ipp')
        return page
