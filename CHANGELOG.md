# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.2]

### Added in version 1.0.2

* Blacklist of networks, connections whom whose IP addresses won't be logged
* Error checking when trying to obtain the external IP address of the honeypot
* Added a verbose option, controlling whether the full IPP request is logged
* Added output plugins for
  * Datadog
  * Discord
  * Kafka
  * NLCV-BAS honeypot data aggregation API
  * RethinkDB
  * Slack
  * Socket
  * Telegram
  * XMPP

### Changed in version 1.0.2

* Updated the documentation
* Improved the unit tests script
* Improved error handling in the plugins that try to open a geolocation database
* Set the executable attribute of the unit tests script
* Fixed a bug in the MySQL and SQLite plugins (the string 'NULL' was passed instead of 0)
* Fixed a typo in the Docker section of the installation documentation
* Got rid of the deprecated functions `utcfromtimestamp` and `utcnow`
* Updated the documentation with information how to start the honeypot at boot
time

## [1.0.1]

### Added in version 1.0.1

* Jobs queue
* Macros for `$job` (current job ID) and `$jobs` (total number of pending jobs)
* Removed `Hold-Job` and `Release-Job` from the list of supported operations
* Added a testing script (Linux only, expects `ipptool` to be installed)
* Emulation of the following IPP operations:
  * `Print-Uri`
  * `Pause-Printer`
  * `Resume-Printer`
  * `Cancel-Job`
  * "cancel current job"
  * `Validate-Job`
  * `Get-Job-Attributes`
  * `Create-Job`
  * `Send-Document`
  * `Send-Uri`

### Changed in version 1.0.1

* Fixed a typo in the `README.md` file
* Require a non-vulnerable version of Twisted

## [1.0.0]

### Added in version 1.0.0

* Initial release
* Implemented the honeypot using the Twisted framework
* Made the honeypot compatible with Python 3.x
* Config file support
* Various command-line options
* Log rotation
* Support for the `report_public_ip` config file option
* A script for starting, stopping, and restarting the honeypot
* Macros like `$ip`, `$now`, `$old`
* Documentation
* Emulation of the following IPP operations:
  * `Get-Printer-Attributes`
  * `Get-Jobs`
  * "get completed jobs"
  * `Print-Job`
* Output plugin support
* Output plugins for
  * CouchDB
  * Elasticsearch
  * HPFeeds
  * Influx 2.0 (Python 3 only)
  * JSON
  * MongoDB
  * MySQL
  * PostgreSQL
  * RedisDB
  * SQLite3
  * syslog
  * text
